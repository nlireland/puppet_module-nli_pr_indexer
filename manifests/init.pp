class pr_indexer (
  $pr_indexer_traject_code_source        = $pr_indexer::params::pr_indexer_traject_code_source,
  $pr_indexer_traject_code_path          = $pr_indexer::params::pr_indexer_traject_code_path ,
  $pr_indexer_cron_hour                  = $pr_indexer::params::pr_indexer_cron_hour,
  $pr_indexer_cron_min                   = $pr_indexer::params::pr_indexer_cron_min,
  $pr_indexer_page_level_metadata_source = $pr_indexer::params::pr_indexer_page_level_metadata_source,
  $pr_indexer_geo_data_source            = $pr_indexer::params::pr_indexer_geo_data_source,
  $pr_indexer_solr_host                  = $pr_indexer::params::pr_indexer_solr_host,
  $pr_indexer_solr_port                  = $pr_indexer::params::pr_indexer_solr_port,
  $pr_indexer_cron_user                  = $pr_indexer::params::pr_indexer_cron_user,
  $pr_indexer_masked_registers           = $pr_indexer::params::pr_indexer_masked_registers,
  $bitbucket_login                       = $pr_indexer::params::bitbucket_login,
  $bitbucket_password                    = $pr_indexer::params::bitbucket_password,
  $marc_auth_file                        = $pr_indexer::params::marc_auth_file,
  $marc_bib_file                         = $pr_indexer::params::marc_bib_file,
  $image_metadata_folder_name            = $pr_indexer::params::image_metadata_folder_name,
  $ruby_version                          = $pr_indexer::params::ruby_version,
) inherits pr_indexer::params {
  $required_packages = ['ruby-devel']
  # traject
  # needs to be installed after rvm and via its own bash shell.
  # to be aware of rvm/ruby/gem settings.
  exec {'json-gem':
    command => "bash -c 'source /etc/profile; /usr/local/rvm/wrappers/default/gem install json'",
    logoutput => 'true',
    path => 'usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin',
    unless => "test -f /usr/local/rvm/gems/$ruby_version/bin/traject",
    require => [Class['rvm'],Package[$required_packages], Rvm_system_ruby [ $ruby_version]],
    #user => $pr_indexer_cron_user,
  }
  exec {'traject':
    command => "bash -c 'source /etc/profile; /usr/local/rvm/wrappers/default/gem install traject'",
    logoutput => 'true',
    path => 'usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin',
    creates => "/usr/local/rvm/gems/$ruby_version/bin/traject",
    require => [Exec["json-gem"],Class['rvm'],Package[$required_packages], Rvm_system_ruby [ $ruby_version]],
    #user => $pr_indexer_cron_user,
  }
  #package {'traject':
  #  provider => 'gem',
  #  name     => 'traject',
  #  ensure   => 'present',
  #  require => [Class['rvm'],Package[$required_packages]],
  #}
  file {"netrc":
    ensure => present,
    content => template('pr_indexer/.netrc.erb'),
    path => "/root/.netrc",
    owner => "root",
    group => "root",
  }
  vcsrepo { "$pr_indexer_traject_code_path":
    ensure   => latest,
    provider => git,
    require  => [File["netrc"], Package["git"] ],
    source   => $pr_indexer_traject_code_source,
    revision => 'master',
  }
  file { "traject_files":
    path => "$pr_indexer_traject_code_path/files",
    ensure => directory,
    owner => "$pr_indexer_cron_user",
    group => "$pr_indexer_cron_user",
    require => [Vcsrepo["$pr_indexer_traject_code_path"]],
  }
  #file {"$pr_indexer_traject_code_path/files/$image_metadata_folder_name":
  #path => "$pr_indexer_traject_code_path/files/$image_metadata_folder_name",
  #  ensure => directory,
  #  require => [Vcsrepo["$pr_indexer_traject_code_path"]],
  #}
  vcsrepo { "$pr_indexer_traject_code_path/files/$image_metadata_folder_name":
    ensure   => latest,
    path       => "$pr_indexer_traject_code_path/files/$image_metadata_folder_name",
    provider => git,
    require  => [ Package["git"], File["traject_files", "netrc"] ],
    source   => $pr_indexer_page_level_metadata_source,
    revision => 'master',
  }
  vcsrepo { "$pr_indexer_traject_code_path/files/geo_data":
    ensure   => latest,
    path       => "$pr_indexer_traject_code_path/files/geo_data",
    provider => git,
    require  => [ Package["git"], File["traject_files", "netrc"] ],
    source   => $pr_indexer_geo_data_source,
    revision => 'master',
  }
  file {"indexer_config.yml":
    ensure => present,
    content => template('pr_indexer/config.yml.erb'),
    path => "$pr_indexer_traject_code_path/config/config.yml",
    require => Vcsrepo["$pr_indexer_traject_code_path"],
  }
# Note: scheduled reindexing the solr index isn't generally necessary and can cause failures. Run the traject script manully if needed.
#  cron { 'traject-index-auth':
#    command => "source /usr/local/rvm/environments/default; ruby $pr_indexer_traject_code_path/lib/index_everything.rb > /tmp/index_everything.log 2>&1",
#    user    => $pr_indexer_cron_user,
#    hour    => $pr_indexer_cron_hour,
#    minute => $pr_indexer_cron_min,
#    environment => 'PATH=usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/sbin',
#    require => [File["config.yml"],Exec["traject"],Vcsrepo["$pr_indexer_traject_code_path"]],
#  }
}
