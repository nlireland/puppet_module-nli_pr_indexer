class pr_indexer::params {
  $pr_indexer_traject_code_source        = "http://bitbucket/.git"
  $pr_indexer_traject_code_path          = "/tmp"
  $pr_indexer_cron_hour                  = 7
  $pr_indexer_cron_min                   = 1
  $pr_indexer_page_level_metadata_source = "http://bitbucket/.git"
  $pr_indexer_geo_data_source            = "http://bitbucket/.git"
  $pr_indexer_solr_host                  = "localhost"
  $pr_indexer_solr_port                  = 8080
  $pr_indexer_cron_user                  = root
  $pr_indexer_masked_registers           = ""
  $bitbucket_login                       = "user"
  $bitbucket_password                    = "password"
  $marc_auth_file                        = "marc_auth_file"
  $marc_bib_file                         = "marc_bib_file"
  $image_metadata_folder_name            = "image_metadata_folder_name"
  $ruby_version                          = "ruby-2.1.5"
}
